;(function($){
    $.fn.showsize = function(options){
        // this
        let elements = this;
        elements.each(function(){
            let opts = $.extend({}, $.fn.showsize.defaults, options, $(this).data());
            $(this).click(function(){
                let msg = $(this).width() + 'x' + $(this).height();
                $(this).wrap('<div style="position:relative;"></div>');
                let div = $('<div>')
                    .text(msg)
                    .css('position', 'absolute')
                    .css('top', '0')
                    .css('background', 'black')
                    .css('color', getRandomColor())
                    .css('font-size', opts.size + 'px')
                    .css('opacity', opts.opacity)
                    .css('padding', '2px');
                $(this).after(div);
            });
        });
    return this;
    };

    $.fn.showalt = function(options){
        let elements = this;
        $(this).click(function(){
            let msg = $(this).attr('alt');
            if(msg === void 0){
                msg = "属性は指定されていません";
            }

            $(this).wrap('<div style="position:relative;"></div>');
            let div = $('<div>')
                .text(msg)
                .css('position', 'absolute')
                .css('bottom', '0')
                .css('background', 'black')
                .css('color', 'white')
                .css('font-size', '20px')
                .css('opacity', '0.9')
                .css('padding', '2px');
            $(this).after(div);
        });

    return this;
    };

    function getRandomColor(){
        let colors = ['white', 'green', 'pink'];
        return colors[Math.floor(Math.random()*colors.length)];
    }

    $.fn.showsize.defaults ={
        size: 20,
        opacity: 0.9
    };
})(jQuery);