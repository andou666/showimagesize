イメージサイズを左上に表示させるjQueryプラグイン  



## 概要
  
このプラグインは以下サイトを利用した改変プラグインとなります。  
[jQueryプラグイン入門](https://dotinstall.com/lessons/basic_jquery_plugin)



## 詳細
  
クリック時に画像左上に縦横のサイズを表示させます。



## 使い方
  
以下のように記述することでimgタグに対して反映  
```
<script>
    $(function() {  
        $('img').showsize({});  // 左上にサイズを表示
        $('img').showalt({});  //左下にサイズを表示
    });
</script>
```
  
jQuery 3.x 系でのみ動作を確認しています。



## デモ
  
ありません。
サンプルフォルダにデモファイルを添付。

サンプル用の画像は以下サイトより利用しています  
[いらすとや](https://www.irasutoya.com/)
